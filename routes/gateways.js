const express = require('express');
const router = express.Router();

const gateways = require('../controllers/gateways');
// Hacemos los routers dinamicos, pensando en el futuro y sin necesidad de duplicar código.
router.post('/pay/:gateway', gateways.pay)

router.post('/reimburse/:gateway', gateways.reimburse)

router.post('/partialreimburse/gateway1', gateways.partialReimburseGateway1)

module.exports = router;
