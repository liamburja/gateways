// Config gateways activated
// Controlamos desde un array que gateways estan o no activados, es más facil de tener controlado teniendolo en un archivo de configuración

module.exports = {
  gatewaysActivated: [
    "gateway1",
    "gateway2"
  ]
}
