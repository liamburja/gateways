const express = require('express');
const app = express();

const cors = require('cors');
app.use(cors());

const routesGateways = require('./routes/gateways');
app.use('/', routesGateways);


//CONNECT PORT
app.set('puerto', process.env.PORT || 3000);

app.listen(app.get('puerto'), function() {
  console.log('Puerto:' + app.get('puerto'));
})
