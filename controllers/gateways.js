const Gateway1 = require('../services/gateway1');
const Gateway2 = require('../services/gateway2');
const config = require('../config');

module.exports = {
  pay: (req, res) => {
    // Recogemos el gateway que proviene de la llamada que realizamos desde el routes
    const gatewayType = req.params.gateway;

    let messageConfirmed;
    // Verificamos que gateway viene y adicionalmente verificar que esta activado
    if (gatewayType === 'gateway1' && config.gatewaysActivated.includes("gateway1")) {
      const gateway1 = new Gateway1();
      messageConfirmed = gateway1.pay();

      return res.status(200).json({
        mensaje: messageConfirmed
      })

    } else if (gatewayType === 'gateway2' && config.gatewaysActivated.includes("gateway2")) {
      const gateway2 = new Gateway2();
      messageConfirmed = gateway2.pay();

      return res.status(200).json({
        mensaje: messageConfirmed
      })
    }
      // Si falla la llamada del gateway devolver un 404 indicando que no esta activado
      return res.status(404).json({
        mensaje: `${gatewayType} not activated`
      })

  },
  reimburse: (req, res) => {
    const gatewayType = req.params.gateway;

    let messageConfirmed;

    if (gatewayType === 'gateway1' && config.gatewaysActivated.includes("gateway1")) {
      const gateway1 = new Gateway1();
      messageConfirmed = gateway1.reimburse();

      return res.status(200).json({
        mensaje: messageConfirmed
      })

    } else if (gatewayType === 'gateway2' && config.gatewaysActivated.includes("gateway2")) {
      const gateway2 = new Gateway2();
      messageConfirmed = gateway2.reimburse();
      
      return res.status(200).json({
        mensaje: messageConfirmed
      })
    }

    return res.status(404).json({
      mensaje: `${gatewayType} not activated`
    })
  },

  partialReimburseGateway1: (req, res) => {
    if (config.gatewaysActivated.includes("gateway1")) {
      const gateway1 = new Gateway1();
      const messageConfirmed = gateway1.partialReimburse(); 
      
      return res.status(200).json({
        mensaje: messageConfirmed
      })
    }

    return res.status(404).json({
      mensaje: `${gatewayType} not activated`
    })
  }
}
