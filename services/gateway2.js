// Manejamos los mensajes que van a salir en cada gateway por individual para esclarecer y tener mejor ordenado todo.
const Gateway = require('./gateway');

class Gateway2 extends Gateway {
  pay() {
    return 'Pay Gateway 2 Completed'
  }

  reimburse() {
    return 'Reimburse Gateway 2 Completed'
  }
}

module.exports = Gateway2
