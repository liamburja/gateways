const Gateway = require('./gateway');
// Manejamos los mensajes que van a salir en cada gateway por individual para esclarecer y tener mejor ordenado todo. Ademas de declarar en este caso una función adicional para este gateway en concreto.
class Gateway1 extends Gateway {
  pay () {
    return 'Pay Gateway 1 Completed'
  }

  reimburse() {
    return 'Reimburse Gateway 1 Completed'
  }

  partialReimburse () {
    return 'Partial Reimburse Gateway 1 Completed'
  }
}


module.exports = Gateway1;
