// Global Class with shared functions
// Creamos una clase especifica para las funciones que todos los gateways van a tener en común con un mensaje a devolver siempre.
class Gateway {
  pay() {
    return 'This function not implemented'
  }

  reimburse() {
    return 'This function not implemented'
  }
}

module.exports = Gateway

