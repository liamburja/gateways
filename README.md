**CONSULTAS MONGO:**

1 - [
  {
    '$project': {
      '_id': 0, 
      'name': '$name', 
      'spawn_time': '$spawn_time', 
      'next_evolution': {
        '$cond': {
          'if': {
            '$isArray': '$next_evolution'
          }, 
          'then': {
            '$size': '$next_evolution'
          }, 
          'else': 0
        }
      }
    }
  }, {
    '$match': {
      'next_evolution': {
        '$gte': 1
      }
    }
  }
]

2 - [
  {
    '$match': {
      'prev_evolution.0': {
        '$exists': false
      }, 
      'avg_spawns': {
        '$gt': 4
      }
    }
  }, {
    '$project': {
      '_id': 0, 
      'name': '$name', 
      'num': '$num'
    }
  }
]



**PRUEBA TECNICA**

Implementación backoffice con diferentes pasarelas de pagos, realizandola de manera que se puedan activar o desactivar facilmente, de forma escalable y facil pensando en un futuro para la implementación de nuevas pasarelas de pagos.
